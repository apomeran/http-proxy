package ar.edu.itba.pdc.proxy;


public class HTTPProxy {
	public static void main(String[] args) {
		HttpServerSelector httpsrv = new HttpServerSelector();
		try {
			httpsrv.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}